#!/bin/bash

ProjectName="ConstPort"
CompilePlatform=1
CompileApplication=1
CopyToDataDirectory=1
DebugBuild=0

if [ $DebugBuild -gt 0 ]
then
	DebugDepCompilerFlags="-DDEBUG=1"
	DebugDepLibDirs="-L../../../lib/glfw/build/src/Debug -L../lib/linux/debug"
else
	DebugDepCompilerFlags="-DDEBUG=0"
	DebugDepLibDirs="-L../../../lib/glfw/build/src/Release -L../lib/linux/release"
fi

# sudo ./bjam cxxflags=-fPIC cflags=-fPIC -a regex runtime-debugging=off variant=release runtime-link=static link=static threading=multi address-model=64

PlatformMainFile="../code/lin_main.cpp"
ApplicationMainFile="../code/app.cpp"
CompilerFlags="-g -DLINUX_COMPILATION -DDOUBLE_RESOLUTION=0 -DDOUBLE_MOUSE_POS=0 -Wno-format-security"
LinkerFlags="-std=gnu++0x"
IncludeDirectories="-I../../../lib/mylib -I../../../lib/glfw/include -I../../../lib/stb -I../../../lib/jsmn -I../../../lib/boost_1_65_1/"
LibraryDirectories=" -L../../../lib/glew/lib"
Libraries="-lboost_regex -ldl -lGL -lm -lGLU `pkg-config --static --libs x11 xrandr xi xxf86vm glew glfw3`"

if [ $CompilePlatform -gt 0 ]
then
	
	python ../IncrementVersionNumber.py ../code/lin_version.h
	# echo [Building Linux Platform]
	g++ $CompilerFlags $DebugDepCompilerFlags $PlatformMainFile -o $ProjectName.exe $IncludeDirectories $LibraryDirectories $Libraries $LinkerFlags $DebugDepLibDirs
	
	if [ $? -ne 0 ]
	then
	    echo Platform Build Failed!
	else
		echo Platform Build Succeeded!
	fi
	
	echo 
	
	if [ $CopyToDataDirectory -gt 0 ]
	then
		cp $ProjectName.exe ../data/$ProjectName.exe
	fi
fi

if [ $CompileApplication -gt 0 ]
then
	
	python ../IncrementVersionNumber.py ../code/appVersion.h
	# echo [Building Linux Application]
	g++ -shared -fvisibility=hidden -fPIC $CompilerFlags $DebugDepCompilerFlags $ApplicationMainFile -o $ProjectName.dll $IncludeDirectories $LibraryDirectories $Libraries $LinkerFlags $DebugDepLibDirs
	
	if [ $? -ne 0 ]
	then
	    echo Application Build Failed!
	else
		echo Application Build Succeeded!
	fi
	
	echo
	
	if [ $CopyToDataDirectory -gt 0 ]
	then
		cp $ProjectName.dll ../data/$ProjectName.dll
	fi
fi
