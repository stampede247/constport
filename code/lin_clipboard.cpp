/*
File:   lin_clipboard.cpp
Author: Taylor Robbins
Date:   03\02\2018
Description: 
	** Holds the functions that handle interfacing with the Linux clipboard

#included from osx_main.cpp
*/

// +==============================+
// |     LIN_CopyToClipboard      |
// +==============================+
// void CopyToClipboard(const void* dataPntr, u32 dataSize)
CopyToClipboard_DEFINITION(LIN_CopyToClipboard)
{
	char* tempSpace = (char*)malloc(dataSize+1);
	memcpy(tempSpace, dataPntr, dataSize);
	tempSpace[dataSize] = '\0';
	
	glfwSetClipboardString(PlatformInfo.window, tempSpace);
	
	free(tempSpace);
}

// +==============================+
// |    LIN_CopyFromClipboard     |
// +==============================+
// void* CopyFromClipboard(MemoryArena_t* arenaPntr, u32* dataLengthOut)
CopyFromClipboard_DEFINITION(LIN_CopyFromClipboard)
{
	*dataLengthOut = 0;
	
	const char* contents = glfwGetClipboardString(PlatformInfo.window);
	if (contents == nullptr) { return nullptr; }
	
	*dataLengthOut = (u32)strlen(contents);

	return (void*)contents;
}

