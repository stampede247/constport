/*
File:   lin_threading.cpp
Author: Taylor Robbins
Date:   03\02\2018
Description: 
	** Holds the platform layer functions that provide the application with an
	** interface to start, pause, stop, and read from system threads. 
*/

// +==============================+
// |      Win32_StartThread       |
// +==============================+
// AppThread_t StartThread(ThreadFunction_f* threadFunction, void* threadInput, ThreadCallback_f* threadCallback)
StartThread_DEFINITION(Win32_StartThread)
{
	AppThread_t result = {};
	
	result.threadFunction = threadFunction;
	result.threadCallback = threadCallback;
	result.threadInput = threadInput;
	
	//TODO: Try opening the thread
	
	return result;
}

// +==============================+
// |      Win32_CloseThread       |
// +==============================+
// void CloseThread(AppThread_t* thread, bool doCallback)
CloseThread_DEFINITION(Win32_CloseThread)
{
	Assert(thread != nullptr);
	
	//TODO: Stop and close the thread
	
	ClearPointer(thread);
}

// +==============================+
// |    Win32_GetThreadStatus     |
// +==============================+
// AppThreadStatus_t GetThreadStatus(AppThread_t* thread)
GetThreadStatus_DEFINITION(Win32_GetThreadStatus)
{
	Assert(thread != nullptr);
	
	AppThreadStatus_t result = AppThreadStatus_Unknown;
	
	//TODO: Query the thread
	
	return result;
}
