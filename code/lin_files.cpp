/*
File:   lin_files.cpp
Author: Taylor Robbins
Date:   03\02\2018
Description: 
	** None 
*/

// +==============================+
// |      LIN_FreeFileMemory      |
// +==============================+
//  void FreeFileMemory(FileInfo_t* fileInfo)
FreeFileMemory_DEFINITION(LIN_FreeFileMemory)
{
	if (fileInfo == nullptr) { return; }
	if (fileInfo->content == nullptr) { return; }
	
	free(fileInfo->content);
	fileInfo->content = nullptr;
	fileInfo->size = 0;
}

// +==============================+
// |      LIN_ReadEntireFile      |
// +==============================+
//  FileInfo_t ReadEntireFile(const char* filename)
ReadEntireFile_DEFINITION(LIN_ReadEntireFile)
{
	FileInfo_t result = {};
	FILE* fileHandle;
	
	if (filename[0] != '/')
	{
		char absolutePath[256] = {};
		snprintf(absolutePath, sizeof(absolutePath), "%s/%s", WorkingDirectory, filename);
		printf("Attempting to open \"%s\"\n", absolutePath);
		fileHandle = fopen(absolutePath, "rb");
	}
	else
	{
		printf("Attempting to open \"%s\"\n", filename);
		fileHandle = fopen(filename, "rb");
		
	}
	
	
	if (fileHandle != nullptr)
	{
		fseek(fileHandle, 0, SEEK_END);
		result.size = ftell(fileHandle);
		result.content = malloc(result.size+1);
		
		fseek(fileHandle, 0, SEEK_SET);
		fread(result.content, 1, result.size, fileHandle);
		
		((u8*)result.content)[result.size] = '\0';
		
		fclose(fileHandle);
	}
	else
	{
		printf("Failed to open file, errno = %s\n", GetErrnoName(errno));
	}
	
	// printf("ReadEntireFile(\"%s\") result: %u bytes %p\n", filename, result.size, result.content);
	return result;
}

// +==============================+
// |     LIN_WriteEntireFile      |
// +==============================+
// bool32 WriteEntireFile(const char* filename, void* memory, uint32 memorySize)
WriteEntireFile_DEFINITION(LIN_WriteEntireFile)
{
	FILE* fileHandle;
	
	if (filename[0] != '/')
	{
		char absolutePath[256] = {};
		snprintf(absolutePath, sizeof(absolutePath), "%s/%s", WorkingDirectory, filename);
		printf("Attempting to save to \"%s\"\n", absolutePath);
		fileHandle = fopen(absolutePath, "wb");
	}
	else
	{
		printf("Attempting to save to \"%s\"\n", filename);
		fileHandle = fopen(filename, "wb");
	}
	
	if (fileHandle == nullptr)
	{
		LIN_WriteLine("Failed to open file");
		return false;
	}
	
	size_t writeResult = fwrite(memory, 1, (size_t)memorySize, fileHandle);
	
	fclose(fileHandle);
	
	if (writeResult != memorySize)
	{
		return false;
	}
	else
	{
		return true;
	}
}

// +==============================+
// |         LIN_OpenFile         |
// +==============================+
// bool32 OpenFile(const char* fileName, OpenFile_t* openFileOut)
OpenFile_DEFINITION(LIN_OpenFile)
{
	ClearPointer(openFileOut);
	
	//TODO: Open the file
	
	return false;
}

// +==============================+
// |        LIN_AppendFile        |
// +==============================+
// bool32 AppendFile(OpenFile_t* filePntr, const void* newData, u32 newDataSize)
AppendFile_DEFINITION(LIN_AppendFile)
{
	//TODO: Append the file
	
	return false;
}

// +==============================+
// |        LIN_CloseFile         |
// +==============================+
// void CloseFile(OpenFile_t* filePntr)
CloseFile_DEFINITION(LIN_CloseFile)
{
	//TODO: Close the file handle
	
	ClearPointer(filePntr);
}

// +==============================+
// |        LIN_LaunchFile        |
// +==============================+
// bool32 LaunchFile(const char* filename)
LaunchFile_DEFINITION(LIN_LaunchFile)
{
	char printBuffer[256];
	if (filename[0] != '/')
	{
		snprintf(printBuffer, sizeof(printBuffer), "open \"%s/%s\"", WorkingDirectory, filename);
	}
	else
	{
		snprintf(printBuffer, sizeof(printBuffer), "open \"%s\"", filename);
	}
	
	pid_t launchPid = popen2(printBuffer, nullptr, nullptr);
	
	if (launchPid > 0)
	{
		return true;
	}
	else
	{
		LIN_PrintLine("popen2 failed, errno = %s", GetErrnoName(errno));
		return false;
	}
}

