/*
File:   lin_helpers.h
Author: Taylor Robbins
Date:   03\02\2018
*/

#ifndef _LIN_HELPERS_H
#define _LIN_HELPERS_H

struct FileInfo_t
{
	uint32 size;
	void* content;
};

struct OpenFile_t
{
	bool isOpen;
	//TODO: Add a handle?
};

#endif //  _LIN_HELPERS_H
