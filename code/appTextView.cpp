/*
File:   appView.cpp
Author: Taylor Robbins
Date:   04\28\2018
Description: 
	** Holds functions that handle updating and rendering the text viewing area 
*/

void UpdateTextView(UiElements_t* ui)
{
	// +==============================+
	// |     Arrow Key Scrolling      |
	// +==============================+
	if (GC->showInputTextBox == false)
	{
		if (ButtonDown(Button_Right))
		{
			ui->scrollOffsetGoto.x += ButtonDown(Button_Shift) ? 16 : 5;
		}
		if (ButtonDown(Button_Left))
		{
			ui->scrollOffsetGoto.x -= ButtonDown(Button_Shift) ? 16 : 5;
		}
		if (ButtonDown(Button_Down))
		{
			ui->scrollOffsetGoto.y += ButtonDown(Button_Shift) ? 16 : 5;
		}
		if (ButtonDown(Button_Up))
		{
			ui->scrollOffsetGoto.y -= ButtonDown(Button_Shift) ? 16 : 5;
			ui->followingEndOfFile = false;
		}
	}
	
	// +==============================+
	// | Mouse Scroll Wheel Handling  |
	// +==============================+
	if (!ButtonDown(Button_Control))
	{
		if (input->scrollDelta.y != 0)
		{
			if (ButtonDown(Button_Shift))
			{
				ui->scrollOffsetGoto.x -= input->scrollDelta.y * (r32)GC->scrollMultiplier;
			}
			else
			{
				ui->scrollOffsetGoto.y -= input->scrollDelta.y * (r32)GC->scrollMultiplier;
				
				if (input->scrollDelta.y > 0)
				{
					ui->followingEndOfFile = false;
				}
			}
		}
		if (input->scrollDelta.x != 0)
		{
		ui->scrollOffsetGoto.x -= input->scrollDelta.x * (r32)GC->scrollMultiplier;
		}
	}
	
	// +==============================+
	// |   Goto End Button Pressed    |
	// +==============================+
	bool gotoEndButtonPressed = false;
	if (IsInsideRec(ui->gotoEndButtonRec, RenderMousePos) && input->mouseInsideWindow && !ui->mouseInMenu)
	{
		output->cursorType = Cursor_Pointer;
		if (ButtonReleased(MouseButton_Left) && IsInsideRec(ui->gotoEndButtonRec, RenderMouseStartPos))
		{
			gotoEndButtonPressed = true;
		}
	}
	if (gotoEndButtonPressed || ButtonPressed(Button_End))
	{
		ui->followingEndOfFile = true;
	}
	
	// +==================================+
	// | Home PageUp and PageDown Hotkeys |
	// +==================================+
	if (ButtonPressed(Button_Home))
	{
		ui->scrollOffsetGoto.y = 0;
		ui->followingEndOfFile = false;
	}
	if (ButtonPressed(Button_PageUp))
	{
		ui->scrollOffsetGoto.y -= ui->viewRec.height;
		ui->followingEndOfFile = false;
	}
	if (ButtonPressed(Button_PageDown))
	{
		ui->scrollOffsetGoto.y += ui->viewRec.height;
	}
	
	//+==========================================+
	//| Scrollbar Interaction and Text Selection |
	//+==========================================+
	if (ButtonDown(MouseButton_Left) && !ui->mouseInMenu)
	{
		//Handle scrollbar interaction with mouse
		if (IsInsideRec(ui->scrollBarGutterRec, RenderMouseStartPos) &&
			ui->scrollBarRec.height < ui->scrollBarGutterRec.height)
		{
			if (input->buttons[MouseButton_Left].transCount > 0)//Pressed the button down
			{
				ui->mouseScrollbarOffset = RenderMousePos.y - ui->scrollBarRec.y;
				if (IsInsideRec(ui->scrollBarRec, RenderMousePos))
				{
					ui->startedOnScrollbar = true;
				}
				else
				{
					ui->startedOnScrollbar = false;
					if (ui->mouseScrollbarOffset > 0)
					{
						ui->scrollOffsetGoto.y += ui->viewRec.height;
					}
					else
					{
						ui->scrollOffsetGoto.y -= ui->viewRec.height;
						ui->followingEndOfFile = false;
					}
				}
			}
			else if (ui->startedOnScrollbar) //holding the button
			{
				r32 newPixelLocation = RenderMousePos.y - ui->mouseScrollbarOffset - ui->scrollBarGutterRec.y;
				if (newPixelLocation > ui->scrollBarGutterRec.y + (ui->scrollBarGutterRec.height - ui->scrollBarRec.height))
				{
					newPixelLocation = ui->scrollBarGutterRec.y + (ui->scrollBarGutterRec.height - ui->scrollBarRec.height);
				}
				if (newPixelLocation < 0)
				{
					newPixelLocation = 0;
				}
				
				ui->scrollOffset.y = (newPixelLocation / (ui->scrollBarGutterRec.height - ui->scrollBarRec.height)) * ui->maxScrollOffset.y;
				ui->scrollOffsetGoto.y = ui->scrollOffset.y;
				
				if (ui->scrollOffsetGoto.y < ui->maxScrollOffset.y - app->mainFont.lineHeight)
				{
					ui->followingEndOfFile = false;
				}
			}
		}
		else if (IsInsideRec(ui->viewRec, RenderMouseStartPos))
		{
			if (input->buttons[MouseButton_Left].transCount > 0)//Pressed the button down
			{
				app->selectionStart = ui->mouseTextLocation;
				app->selectionEnd = ui->mouseTextLocation;
			}
			else //if (IsInsideRec(RenderMousePos, ui->viewRec)) //Mouse Button Holding
			{
				app->selectionEnd = ui->mouseTextLocation;
			}
		}
	}
	
	//+================================+
	//|  Context Menu Showing/Filling  |
	//+================================+
	Menu_t* contextMenu = GetMenuByName(&app->menuHandler, "Context Menu");
	contextMenu->show = false;
	if (ui->mouseInMenu == false &&
		ButtonDown(Button_Control) &&// && mousePos.x <= ui->gutterRec.width)
		(IsInsideRec(ui->viewRec, RenderMousePos) || IsInsideRec(ui->gutterRec, RenderMousePos)))
	{
		Line_t* linePntr = LineListGetItemAt(&app->lineList, ui->mouseTextLocation.lineIndex);
		
		if (linePntr != nullptr && linePntr->timestamp != 0)
		{
			RealTime_t lineTime;
			GetRealTime(linePntr->timestamp, true, &lineTime);
			
			if (ButtonDown(Button_Shift))
			{
				ui->contextString = TempPrint("%s %u:%02u:%02u%s (%s %s, %u)",
					GetDayOfWeekStr((DayOfWeek_t)lineTime.dayOfWeek),
					Convert24HourTo12Hour(lineTime.hour), lineTime.minute, lineTime.second,
					IsPostMeridian(lineTime.hour) ? "pm" : "am",
					GetMonthStr((Month_t)lineTime.month), GetDayOfMonthString(lineTime.day), lineTime.year
				);
			}
			else
			{
				i64 secondsDifference = SubtractTimes(platform->localTime, lineTime, TimeUnit_Seconds);
				i64 absDifference = AbsI64(secondsDifference);
				ui->contextString = TempPrint("%s Ago", GetElapsedString((u64)absDifference));
			}
			
			contextMenu->show = true;
		}
	}
	
	//+================================+
	//|   Mark lines using the mouse   |
	//+================================+
	if (ui->mouseInMenu == false &&
		IsInsideRec(ui->gutterRec, RenderMousePos) && IsInsideRec(ui->gutterRec, RenderMouseStartPos))
	{
		if (ButtonReleased(MouseButton_Left) &&
			ui->markIndex >= 0 && ui->markIndex < app->lineList.numLines)
		{
			Line_t* linePntr = LineListGetItemAt(&app->lineList, ui->markIndex);
			
			if (!IsFlagSet(linePntr->flags, LineFlag_MarkBelow) ||
				(ButtonDown(Button_Shift) && !IsFlagSet(linePntr->flags, LineFlag_ThickMark)))
			{
				FlagSet(linePntr->flags, LineFlag_MarkBelow);
				if (ButtonDown(Button_Shift))
				{
					FlagSet(linePntr->flags, LineFlag_ThickMark);
				}
				else
				{
					FlagUnset(linePntr->flags, LineFlag_ThickMark);
				}
			}
			else
			{
				FlagUnset(linePntr->flags, LineFlag_MarkBelow);
			}
		}
	}
	
	// +==============================+
	// |       Show Text Cursor       |
	// +==============================+
	if (IsInsideRec(ui->viewRec, RenderMousePos) && !ui->mouseInMenu && GC->showTextCursor)
	{
		output->cursorType = Cursor_Text;
	}
	
}
