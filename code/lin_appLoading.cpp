/*
File:   lin_appLoading.cpp
Author: Taylor Robbins
Date:   03\02\2018
Description: 
	** Contains functions and types related to loading the application DLL

#included from lin_main.cpp 
*/

struct LoadedApp_t
{
	bool isValid;
	bool reinitializeApp;
	Version_t version;
	void* handle;
	time_t lastWriteTime;
	
	AppGetVersion_f* GetVersion;
	AppInitialize_f* Initialize;
	AppReloading_f*  Reloading;
	AppReloaded_f*   Reloaded;
	AppUpdate_f*     Update;
	AppClosing_f*    Closing;
};

// +--------------------------------------------------------------+
// |                      App Stub Functions                      |
// +--------------------------------------------------------------+
AppGetVersion_DEFINITION(AppGetVersion_Stub)
{
	Version_t result = {};
	return result;
}
AppReloaded_DEFINITION(AppReloading_Stub)
{
	
}
AppReloaded_DEFINITION(AppReloaded_Stub)
{
	
}
AppInitialize_DEFINITION(AppInitialize_Stub)
{
	
}
AppUpdate_DEFINITION(AppUpdate_Stub)
{
	
}
AppClosing_DEFINITION(AppClosing_Stub)
{
	
}

// +--------------------------------------------------------------+
// |                       Public Functions                       |
// +--------------------------------------------------------------+
time_t GetLastModifiedTime(const char* filePath)
{
	struct stat fileAttributes;
	stat(filePath, &fileAttributes);
	return fileAttributes.st_mtime;
}

bool LoadDllCode(const char* appDllPath, const char* tempDllPath, LoadedApp_t* loadedApp)
{
	ClearPointer(loadedApp);
	
	loadedApp->lastWriteTime = GetLastModifiedTime(appDllPath);
	
	#if DEBUG
	{
		char copyCommand[256] = {};
		snprintf(copyCommand, sizeof(copyCommand), "cp %s %s", appDllPath, tempDllPath);
		system(copyCommand);
		
		loadedApp->handle = dlopen(tempDllPath, RTLD_NOW);
	}
	#else
	{
		loadedApp->handle = dlopen(appDllPath, RTLD_NOW);
	}
	#endif
	
	if (loadedApp->handle != nullptr)
	{
		loadedApp->GetVersion = (AppGetVersion_f*) dlsym(loadedApp->handle, "App_GetVersion");
		loadedApp->Initialize = (AppInitialize_f*) dlsym(loadedApp->handle, "App_Initialize");
		loadedApp->Reloading  = (AppReloading_f*)  dlsym(loadedApp->handle, "App_Reloading");
		loadedApp->Reloaded   = (AppReloaded_f*)   dlsym(loadedApp->handle, "App_Reloaded");
		loadedApp->Update     = (AppUpdate_f*)     dlsym(loadedApp->handle, "App_Update");
		loadedApp->Closing    = (AppClosing_f*)    dlsym(loadedApp->handle, "App_Closing");
		
		loadedApp->isValid = (
			loadedApp->GetVersion != nullptr &&
			loadedApp->Initialize != nullptr &&
			loadedApp->Reloading != nullptr &&
			loadedApp->Reloaded != nullptr &&
			loadedApp->Update != nullptr &&
			loadedApp->Closing != nullptr
		);
		
		if (!loadedApp->isValid)
		{
			LIN_WriteLine("Unable to get all application functions");
		}
	}
	else
	{
		LIN_PrintLine("dlopen failed: %s", dlerror());
	}
	
	if (loadedApp->isValid == false)
	{
		loadedApp->GetVersion = AppGetVersion_Stub;
		loadedApp->Initialize = AppInitialize_Stub;
		loadedApp->Reloading  = AppReloading_Stub;
		loadedApp->Reloaded   = AppReloaded_Stub;
		loadedApp->Update     = AppUpdate_Stub;
		loadedApp->Closing    = AppClosing_Stub;
	}
	
	loadedApp->version = loadedApp->GetVersion(&loadedApp->reinitializeApp);
	
	return loadedApp->isValid;
}

void FreeDllCode(LoadedApp_t* loadedApp)
{
	if (loadedApp != nullptr &&
		loadedApp->handle != nullptr)
	{
		dlclose(loadedApp->handle);
	}
	
	ClearPointer(loadedApp);
}
