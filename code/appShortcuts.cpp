/*
File:   appShortcuts.cpp
Author: Taylor Robbins
Date:   04\21\2018
Description: 
	** Holds the code that handles all the application shortcuts. These functions simply get called from AppUpdate
*/

// +--------------------------------------------------------------+
// |                       Public Functions                       |
// +--------------------------------------------------------------+
//NOTE: This is called before everything in AppUpdate
void HandleShortcutsBefore(UiElements_t* ui)
{
	// +==============================+
	// |      Start Test Thread       |
	// +==============================+
	if (ButtonPressed(Button_F2) && app->testThread.started == false)
	{
		app->testThread = platform->StartThread(TestThreadFunction, app, nullptr);
		if (app->testThread.started)
		{
			DEBUG_WriteLine("Thread Started!");
		}
	}
	
	// +==============================+
	// |    Clear Console Shortcut    |
	// +==============================+
	if (ButtonDown(Button_Control) && ButtonDown(Button_Shift) && ButtonPressed(Button_C))
	{
		ClearConsole();
	}
	
	//+==================================+
	//|   Reload Global Configuration    |
	//+==================================+
	if (ButtonPressed(Button_R) && ButtonDown(Button_Control))
	{
		if (ButtonDown(Button_Shift))
		{
			//Reload shaders
			app->simpleShader = LoadShader("Resources/Shaders/simple-vertex.glsl", "Resources/Shaders/simple-fragment.glsl");
			PopupSuccess("Reloaded shaders");
		}
		else
		{
			DisposeGlobalConfig(&app->globalConfig);
			LoadGlobalConfiguration(platform, &app->globalConfig, &app->mainHeap);
			ParseLineBreakConfigOptions();
			DisposeRegexFile(&app->regexList);
			LoadRegexFile(&app->regexList, "Resources/Configuration/RegularExpressions.rgx", &app->mainHeap);
			LoadApplicationFonts();
			
			PopupSuccess("Reloaded application configuration options");
		}
	}
	
	// +==================================+
	// | Font Scaling Using Scroll Wheel  |
	// +==================================+
	if (ButtonDown(Button_Control) && input->scrollDelta.y != 0)
	{
		ui->scrollOffsetGoto.y -= input->scrollDelta.y * (r32)GC->scrollMultiplier;
		GC->mainFontSize += (i32)(input->scrollDelta.y);
		if (GC->mainFontSize < 10) { GC->mainFontSize = 10; }
		if (GC->mainFontSize > 64) { GC->mainFontSize = 64; }
		
		LoadApplicationFonts();
	}
	
	// +==================================+
	// |      Toggle Output To File       |
	// +==================================+
	if (ButtonPressed(Button_O) && ButtonDown(Button_Control))
	{
		char* outputFileName = TempCombine(GetPortUserName(app->comPort.name), "_Output.txt");
		
		DEBUG_PrintLine("Outputting to file: \"%s\"", outputFileName);
		
		if (app->writeToFile)
		{
			platform->CloseFile(&app->outputFile);
			app->writeToFile = false;
			StatusSuccess("Stopped outputting to file");
		}
		else
		{
			if (platform->OpenFile(outputFileName, &app->outputFile))
			{
				StatusSuccess("Opened file successfully");
				app->writeToFile = true;
				const char* newString = "\r\n\r\n[File Opened for Writing]\r\n";
				platform->AppendFile(&app->outputFile, newString, (u32)strlen(newString));
			}
			else
			{
				StatusError("Error opening \"%s\" for writing", outputFileName);
			}
		}
	}
	
	// +==============================+
	// |    Start Python Shortcut     |
	// +==============================+
	if (GC->pythonScriptEnabled && ButtonPressed(Button_P) && ButtonDown(Button_Control))
	{
		if (app->programInstance.isOpen)
		{
			StatusInfo("Closing python instance");
			platform->CloseProgramInstance(&app->programInstance);
		}
		else
		{
			if (GC->pythonScript != nullptr)
			{
				char* commandStr = TempPrint("python %s", GC->pythonScript);
				StatusInfo("Running System Command: \"%s\"", commandStr);
				app->programInstance = platform->StartProgramInstance(commandStr);
				if (app->programInstance.isOpen == false)
				{
					StatusError("Python exec failed: \"%s\"", commandStr);
				}
			}
			else
			{
				StatusError("No python script defined!");
			}
		}
	}
	
	// +================================+
	// |      Toggle Debug Overlay      |
	// +================================+
	if (ButtonPressed(Button_F11))
	{
		app->showDebugMenu = !app->showDebugMenu;
	}
	
	// +==============================+
	// |   Close COM Port Shortcut    |
	// +==============================+
	if (ButtonPressed(Button_W) && ButtonDown(Button_Control))
	{
		if (ButtonDown(Button_Shift))
		{
			output->closeWindow = true;
		}
		else
		{
			if (app->comPort.isOpen)
			{
				char* tempComName = ArenaString(TempArena, NtStr(app->comPort.name));
				platform->CloseComPort(&app->mainHeap, &app->comPort);
				// ClearConsole();
				RefreshComPortList();
				app->comMenu.comListSelectedIndex = app->availablePorts.count;
				PopupError("Closed \"%s\"", tempComName);
			}
		}
	}
	
	//+==================================+
	//| Quick keys for opening COM ports |
	//+==================================+
	if (ButtonDown(Button_Control))
	{
		const char* quickComSelection = nullptr;
		
		if (ButtonReleased(Button_1)) quickComSelection = "COM1";
		if (ButtonReleased(Button_2)) quickComSelection = "COM2";
		if (ButtonReleased(Button_3)) quickComSelection = "COM3";
		if (ButtonReleased(Button_4)) quickComSelection = "COM4";
		if (ButtonReleased(Button_5)) quickComSelection = "COM5";
		if (ButtonReleased(Button_6)) quickComSelection = "COM6";
		if (ButtonReleased(Button_7)) quickComSelection = "COM7";
		if (ButtonReleased(Button_8)) quickComSelection = "COM8";
		if (ButtonReleased(Button_9)) quickComSelection = "COM9";
		
		if (quickComSelection != nullptr)
		{
			RefreshComPortList();
			
			if (IsComAvailable(quickComSelection) == false)
			{
				PopupError("%s not Available!", quickComSelection);
			}
			else
			{
				OpenComPort(quickComSelection, app->comMenuOptions.settings);
			}
		}
	}
	
	// +==============================+
	// |      Create New Window       |
	// +==============================+
	if (ButtonDown(Button_Control) && ButtonDown(Button_Shift) && ButtonPressed(Button_N))
	{
		#if DEBUG
		PopupError("Cannot open new window in debug mode");
		#else
		platform->CreateNewWindow();
		#endif
	}
}

//NOTE: This is called after Data was read and UI was sized, but before COM Menu and Before view movement
void HandleShortcutsMid(UiElements_t* ui)
{
	// +==============================+
	// |      Recall Last Input       |
	// +==============================+
	if (GC->showInputTextBox && ButtonPressed(Button_Up))
	{
		if (app->inputHistoryIndex < app->numHistoryItems)
		{
			app->inputHistoryIndex++;
			DEBUG_PrintLine("Pull history %u", app->inputHistoryIndex);
			TextBoxSet(&app->inputBox, NtStr(&app->inputHistory[app->inputHistoryIndex-1][0]));
		}
	}
	if (GC->showInputTextBox && ButtonPressed(Button_Down))
	{
		if (app->inputHistoryIndex > 1)
		{
			app->inputHistoryIndex--;
			DEBUG_PrintLine("Pull history %u", app->inputHistoryIndex);
			TextBoxSet(&app->inputBox, NtStr(&app->inputHistory[app->inputHistoryIndex-1][0]));
		}
		else
		{
			app->inputHistoryIndex = 0;
			DEBUG_PrintLine("Pull history %u", app->inputHistoryIndex);
			TextBoxClear(&app->inputBox);
		}
	}
	
	// +==============================+
	// | Copy Selection to Clipboard  |
	// +==============================+
	if ((IsActiveElement(&ui->viewRec) || (GC->showInputTextBox && IsActiveElement(&app->inputBox))) && ButtonDown(Button_Control) && ButtonPressed(Button_C) && !ButtonDown(Button_Shift))
	{
		u32 selectionSize = GetSelection(app->selectionStart, app->selectionEnd, false);
		if (selectionSize != 0)
		{
			TempPushMark();
			
			char* selectionTempBuffer = TempString(selectionSize+1);
			GetSelection(app->selectionStart, app->selectionEnd, false, selectionTempBuffer);
			
			platform->CopyToClipboard(selectionTempBuffer, selectionSize);
			StatusSuccess("Copied %u bytes to clipboard", selectionSize);
			
			TempPopMark();
		}
	}
	
	//+==================================+
	//|       Select/Deselect all        |
	//+==================================+
	if (ButtonPressed(Button_A) && ButtonDown(Button_Control))
	{
		if (app->selectionStart.lineIndex == app->selectionEnd.lineIndex &&
			app->selectionStart.charIndex == app->selectionEnd.charIndex)
		{
			//Select all
			app->selectionStart = NewTextLocation(0, 0);
			app->selectionEnd = NewTextLocation(app->lineList.numLines-1, app->lineList.lastLine->numChars);
		}
		else
		{
			//Deselect all
			app->selectionStart = NewTextLocation(0, 0);
			app->selectionEnd = NewTextLocation(0, 0);
		}
	}
}

//NOTE: This is called after view movement, just before rendering
void HandleShortcutsAfter(UiElements_t* ui)
{
	// +==============================+
	// |    Debug Output Shortcut     |
	// +==============================+
	if (ButtonDown(Button_Control) && ButtonPressed(Button_QuestionMark))
	{
		DEBUG_PrintLine("RenderScreenSize = (%f, %f)", RenderScreenSize.width, RenderScreenSize.height);
		GLint dims[4] = {0};
		glGetIntegerv(GL_VIEWPORT, dims);
		GLint frameWidth = dims[2];
		GLint frameHeight = dims[3];
		DEBUG_PrintLine("FrameBufferSize = (%d, %d)", frameWidth, frameHeight);
		DEBUG_PrintLine("Mouse Pos = (%f, %f)", RenderMousePos.x, RenderMousePos.y);
		
		if (app->comPort.isOpen)
		{
			r32 framesPerSecond = 60.0f;
			u32 numBytesPerFrame = GetNumBytesInPeriodForComSettings(&app->comPort.settings, (1000000.0f / framesPerSecond));
			DEBUG_PrintLine("%.2fus: %u bytes", (1000000.0f / framesPerSecond), numBytesPerFrame);
		}
		
		DEBUG_Print("Hex Line Break Before Chars: [%u]{ ", app->hexLineBreakBeforeCharCount);
		for (u32 cIndex = 0; cIndex < app->hexLineBreakBeforeCharCount; cIndex++)
		{
			DEBUG_Print("%02X ", app->hexLineBreakBeforeChars[cIndex]);
		}
		DEBUG_WriteLine("}");
		DEBUG_Print("Hex Line Break After Chars: [%u]{ ", app->hexLineBreakAfterCharCount);
		for (u32 cIndex = 0; cIndex < app->hexLineBreakAfterCharCount; cIndex++)
		{
			DEBUG_Print("%02X ", app->hexLineBreakAfterChars[cIndex]);
		}
		DEBUG_WriteLine("}");
		
		DEBUG_PrintLine("AutoNewLineTime = %d", GC->autoNewLineTime);
	}
	
}
