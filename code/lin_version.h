/*
File:   lin_version.h
Author: Taylor Robbins
Date:   03\02\2018
*/

#ifndef _LIN_VERSION_H
#define _LIN_VERSION_H

#define PLATFORM_VERSION_MAJOR    1
#define PLATFORM_VERSION_MINOR    7

#define PLATFORM_VERSION_BUILD    61

#endif //  _LIN_VERSION_H
